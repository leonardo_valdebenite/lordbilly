/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

using GreenshotEditor.Drawing.Fields;
using GreenshotPlugin;
using GreenshotPlugin.Drawing;
using GreenshotEditor.Memento;

namespace GreenshotEditor.Drawing {
	/// <summary>
	/// Represents a textbox (extends RectangleContainer for border/background support
	/// </summary>
	[Serializable] 
	public class TextContainer : RectangleContainer, ITextContainer {
		private bool fontInvalidated = true;
		// If makeUndoable is true the next text-change will make the change undoable.
		// This is set to true AFTER the first change is made, as there is already a "add element" on the undo stack
		private bool makeUndoable = false;
		private Font font;

		/// <summary>
		/// The StringFormat object is not serializable!!
		/// </summary>
		[NonSerialized]
		StringFormat stringFormat;
		
		private string text;
		// there is a binding on the following property!
		public string Text {
			get { return text; }
			set { 
				ChangeText(value, true);
			}
		}
		
		internal void ChangeText(string newText, bool allowUndoable) {
			if ((text == null && newText != null)  || !text.Equals(newText)) {
				if (makeUndoable && allowUndoable) {
					makeUndoable = false;
					parent.MakeUndoable(new TextChangeMemento(this), false);
				}
				text = newText; 
				OnPropertyChanged("Text"); 
			}
		}

		protected bool italic = false;
		[Field(FieldTypes.FONT_ITALIC)]
		public bool Italic {
			get {
				return italic;
			}
			set {
				italic = value;
				OnFieldPropertyChanged(FieldTypes.FONT_ITALIC);
			}
		}

		protected bool bold = false;
		[Field(FieldTypes.FONT_BOLD)]
		public bool Bold {
			get {
				return bold;
			}
			set {
				bold = value;
				OnFieldPropertyChanged(FieldTypes.FONT_BOLD);
			}
		}

		protected float fontSize = 11f;
		[Field(FieldTypes.FONT_SIZE)]
		public float FontSize {
			get {
				return fontSize;
			}
			set {
				fontSize = value;
				OnFieldPropertyChanged(FieldTypes.FONT_SIZE);
			}
		}
		protected string fontFamily = FontFamily.GenericSansSerif.Name;
		[Field(FieldTypes.FONT_FAMILY)]
		public string Family {
			get {
				return fontFamily;
			}
			set {
				fontFamily = value;
				OnFieldPropertyChanged(FieldTypes.FONT_FAMILY);
			}
		}

		protected HorizontalAlignment horizontalAlignment = HorizontalAlignment.Center;
		[Field(FieldTypes.TEXT_HORIZONTAL_ALIGNMENT)]
		public HorizontalAlignment HorizontalAlignment {
			get {
				return horizontalAlignment;
			}
			set {
				horizontalAlignment = value;
				OnFieldPropertyChanged(FieldTypes.TEXT_HORIZONTAL_ALIGNMENT);
			}
		}

		protected VerticalAlignment verticalAlignment = VerticalAlignment.CENTER;
		[Field(FieldTypes.TEXT_VERTICAL_ALIGNMENT)]
		public VerticalAlignment VerticalAlignment {
			get {
				return verticalAlignment;
			}
			set {
				verticalAlignment = value;
				OnFieldPropertyChanged(FieldTypes.TEXT_VERTICAL_ALIGNMENT);
			}
		}

		[NonSerialized]
		private TextBox textBox;
		
		public TextContainer(Surface parent) : base(parent) {
			Init();
			stringFormat = new StringFormat();
			stringFormat.Trimming = StringTrimming.EllipsisWord;
		}
		
		[OnDeserializedAttribute]
		private void OnDeserialized(StreamingContext context) {
			stringFormat = new StringFormat();
			Init();
			UpdateFormat();
		}

		protected override void Dispose(bool disposing) {
			if (disposing) {
				if (font != null) {
					font.Dispose();
					font = null;
				}
				if (stringFormat != null) {
					stringFormat.Dispose();
					stringFormat = null;
				}
				if (textBox != null) {
					textBox.Dispose();
					textBox = null;
				}
			}
			base.Dispose(disposing);
		}
		
		private void Init() {
			CreateTextBox();
			this.PropertyChanged += new PropertyChangedEventHandler(TextContainer_PropertyChanged);
		}
		
		public void FitToText() {
			UpdateFormat();
			Size textSize = TextRenderer.MeasureText(text, font);
			Width = textSize.Width + lineThickness;
			Height = textSize.Height + lineThickness;
		}

		void TextContainer_PropertyChanged(object sender, PropertyChangedEventArgs e) {
			if (e.PropertyName.Equals("Selected")) {
				if (!Selected && textBox.Visible) {
					HideTextBox();
				} else if (Selected && Status==EditStatus.DRAWING) {
					ShowTextBox();
				}
			}
			if (font != null) {
				font.Dispose();
				font = null;
			}
			fontInvalidated = true;
			if (textBox.Visible) {
				UpdateTextBoxPosition();
				UpdateTextBoxFormat();
				textBox.Invalidate();
			} else {
				UpdateFormat();
			}
		}
		
		public override void OnDoubleClick() {
			ShowTextBox();
			textBox.Focus();
		}
		
		private void CreateTextBox() {
			textBox = new TextBox();
            if (this.lineColor.GetBrightness() < 0.4) {
                textBox.BackColor = Color.FromArgb(51,51,51);
            }
            
			textBox.ImeMode = ImeMode.On;
			textBox.Multiline = true;
			textBox.AcceptsTab = true;
			textBox.AcceptsReturn = true;
			textBox.DataBindings.Add("Text", this, "Text", false, DataSourceUpdateMode.OnPropertyChanged);
			textBox.LostFocus += new EventHandler(textBox_LostFocus);
			textBox.KeyDown += new KeyEventHandler(textBox_KeyDown);
			textBox.BorderStyle = BorderStyle.FixedSingle;
			textBox.Visible = false;
		}

		private void ShowTextBox() {
			parent.KeysLocked = true;
			parent.Controls.Add(textBox);
            EnsureTextBoxContrast();
			textBox.Show();
			textBox.Focus();
		}

        /// <summary>
        /// Makes textbox background dark if text color is very bright
        /// </summary>
        private void EnsureTextBoxContrast() {
            Color lc = this.lineColor;
            if (lc.R > 203 && lc.G > 203 && lc.B > 203) {
                textBox.BackColor = Color.FromArgb(51, 51, 51);
            } else {
                textBox.BackColor = Color.White;
            }
        }
		
		private void HideTextBox() {
			parent.Focus();
			textBox.Hide();
			parent.KeysLocked = false;
			parent.Controls.Remove(textBox);
		}
		
		private void UpdateFormat() {
			try {
				if (fontInvalidated && fontFamily != null && fontSize != 0) {
					FontStyle fs = FontStyle.Regular;

					bool hasStyle = false;
					using (FontFamily fam = new FontFamily(fontFamily)) {
						bool boldAvailable = fam.IsStyleAvailable(FontStyle.Bold);
						if (bold && boldAvailable) {
							fs |= FontStyle.Bold;
							hasStyle = true;
						}

						bool italicAvailable = fam.IsStyleAvailable(FontStyle.Italic);
						if (italic && italicAvailable) {
							fs |= FontStyle.Italic;
							hasStyle = true;
						}

						if (!hasStyle) {
							bool regularAvailable = fam.IsStyleAvailable(FontStyle.Regular);
							if (regularAvailable) {
								fs = FontStyle.Regular;
							} else {
								if (boldAvailable) {
									fs = FontStyle.Bold;
								} else if (italicAvailable) {
									fs = FontStyle.Italic;
								}
							}
						}
						font = new Font(fam, fontSize, fs, GraphicsUnit.Pixel);
					}
					fontInvalidated = false;
				}

			} catch (Exception ex) {
				ex.Data.Add("fontFamily", fontFamily);
				ex.Data.Add("fontBold", bold);
				ex.Data.Add("fontItalic", italic);
				ex.Data.Add("fontSize", fontSize);
				throw;
			}
			
			stringFormat.Alignment = (StringAlignment)horizontalAlignment;
			stringFormat.LineAlignment = (StringAlignment)verticalAlignment;
		}
		
		private void UpdateTextBoxPosition() {
			textBox.Left = this.Left;
			textBox.Top = this.Top;
			textBox.Width = this.Width;
			textBox.Height = this.Height;
		}

		public override void ApplyBounds(RectangleF newBounds) {
			base.ApplyBounds(newBounds);
			UpdateTextBoxPosition();
		}

		private void UpdateTextBoxFormat() {
			UpdateFormat();
			textBox.ForeColor = lineColor;
			textBox.Font = font;
		}
		
		void textBox_KeyDown(object sender, KeyEventArgs e) {
			// ESC and Enter/Return (w/o Shift) hide text editor
			if(e.KeyCode == Keys.Escape || ((e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter) &&  e.Modifiers == Keys.None)) {
				HideTextBox();
				e.SuppressKeyPress = true;
			}
		}

		void textBox_LostFocus(object sender, EventArgs e) {
			// next change will be made undoable
			makeUndoable = true;
			HideTextBox();
		}
	
		public override void Draw(Graphics graphics, RenderMode rm) {
			base.Draw(graphics, rm);
			UpdateFormat();
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
			graphics.CompositingQuality = CompositingQuality.HighQuality;
			graphics.PixelOffsetMode = PixelOffsetMode.None;
			graphics.TextRenderingHint = TextRenderingHint.SystemDefault;

			Rectangle rect = GuiRectangle.GetGuiRectangle(this.Left, this.Top, this.Width, this.Height);
			if (Selected && rm == RenderMode.EDIT) {
				DrawSelectionBorder(graphics, rect);
			}
			
			if (text == null || text.Length == 0 ) {
				return;
			}

			// we only draw the shadow if there is no background
			int textOffset = (lineThickness>0) ? (int)Math.Ceiling(lineThickness/2d) : 0;
			// draw shadow before anything else
			if (shadow && (fillColor == Color.Transparent || fillColor == Color.Empty)) {
				int basealpha = 100;
				int alpha = basealpha;
				int steps = 5;
				int currentStep = 1;
				while (currentStep <= steps) {
					int offset = currentStep;
					Rectangle shadowRect = GuiRectangle.GetGuiRectangle(Left + offset, Top + offset, Width, Height);
					if (lineThickness > 0) {
						shadowRect.Inflate(-textOffset, -textOffset);
					}
					using (Brush fontBrush = new SolidBrush(Color.FromArgb(alpha, 100, 100, 100))) {
						graphics.DrawString(text, font, fontBrush, shadowRect, stringFormat);
						currentStep++;
						alpha = alpha - basealpha / steps;
					}
				}
			}
			DrawText(graphics, rect, null);
		}

		protected void DrawText(Graphics graphics, Rectangle drawingRectange, StringFormat stringFormat) {
			UpdateFormat();
			Rectangle fontRect = drawingRectange;
			int textOffset = (lineThickness > 0) ? (int)Math.Ceiling(lineThickness / 2d) : 0;
 			if (lineThickness > 0) {
 				graphics.SmoothingMode = SmoothingMode.HighSpeed;
 				fontRect.Inflate(-textOffset, -textOffset);
 			}
 			graphics.SmoothingMode = SmoothingMode.HighQuality;
 			using (Brush fontBrush = new SolidBrush(lineColor)) {
				graphics.DrawString(text, font, fontBrush, fontRect);
				if (stringFormat != null) {
					graphics.DrawString(text, font, fontBrush, fontRect, stringFormat);
				} else {
					graphics.DrawString(text, font, fontBrush, fontRect);
				}
 			}
		}
		
		public override bool ClickableAt(int x, int y) {
			Rectangle r = GuiRectangle.GetGuiRectangle(Left, Top, Width, Height);
			r.Inflate(5, 5);
			return r.Contains(x, y);
		}
	}
}

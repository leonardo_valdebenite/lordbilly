/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Globalization;
using System.Windows.Forms;

using GreenshotPlugin.Controls;
using GreenshotPlugin.Core;
using Greenshot.IniFile;
using System.Collections.Generic;
using System.Threading;

namespace GreenshotJiraPlugin {
	public partial class JiraForm : Form {
		private JiraRest jiraConnector;
		private Issue selectedIssue;
		private GreenshotColumnSorter columnSorter;
		private JiraConfiguration config = IniConfig.GetIniSection<JiraConfiguration>();

		public JiraForm(JiraRest jiraConnector) {
			InitializeComponent();
			this.Icon = GreenshotPlugin.Core.GreenshotResources.getGreenshotIcon();
			initializeComponentText();

			this.columnSorter = new GreenshotColumnSorter();
			this.jiraListView.ListViewItemSorter = columnSorter;

			this.jiraConnector = jiraConnector;

			changeModus(false);
			uploadButton.Enabled = false;
			updateForm();
		}

		private void initializeComponentText() {
			this.label_jirafilter.Text = Language.GetString("jira", LangKey.label_jirafilter);
			this.label_comment.Text = Language.GetString("jira", LangKey.label_comment);
			this.label_filename.Text = Language.GetString("jira", LangKey.label_filename);
		}

		private void updateForm() {
			var filters = jiraConnector.Filters().Result;
			if (filters != null) {
				foreach (var filter in filters) {
					jiraFilterBox.Items.Add(filter);
				}
				jiraFilterBox.SelectedIndex = 0;
			}
			changeModus(true);
			if (!string.IsNullOrEmpty(config.LastUsedJira)) {
				selectedIssue = jiraConnector.Details(config.LastUsedJira).Result;
				if (selectedIssue != null) {
					jiraKey.Text = config.LastUsedJira;
					uploadButton.Enabled = true;
				}
			}
		}

		private void changeModus(bool enabled) {
			jiraFilterBox.Enabled = enabled;
			jiraListView.Enabled = enabled;
			jiraFilenameBox.Enabled = enabled;
			jiraCommentBox.Enabled = enabled;
		}

		public void setFilename(string filename) {
			jiraFilenameBox.Text = filename;
		}

		public void setComment(string comment) {
			jiraCommentBox.Text = comment;
		}

		public Issue getJiraIssue() {
			return selectedIssue;
		}

		public void upload(IBinaryContainer attachment) {
			config.LastUsedJira = selectedIssue.Key;
			jiraConnector.Attach(selectedIssue.Key, jiraFilenameBox.Text, attachment, new CancellationToken());
			if (jiraCommentBox.Text != null && jiraCommentBox.Text.Length > 0) {
				jiraConnector.Comment(selectedIssue.Key, jiraCommentBox.Text);
			}
		}

		private void selectJiraToolStripMenuItem_Click(object sender, EventArgs e) {
			ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;
			selectedIssue = (Issue)clickedItem.Tag;
			jiraKey.Text = selectedIssue.Key;
		}

		private void jiraFilterBox_SelectedIndexChanged(object sender, EventArgs e) {
			List<Issue> issues = null;
			uploadButton.Enabled = false;
			FilterFavourite filter = (FilterFavourite)jiraFilterBox.SelectedItem;
			if (filter == null) {
				return;
			}
			// Run upload in the background
			new PleaseWaitForm().ShowAndWait("Jira plug-in", Language.GetString("jira", LangKey.communication_wait),
				delegate() {
					issues = jiraConnector.Find(filter.Jql).Result.Issues;
				}
			);
			jiraListView.BeginUpdate();
			jiraListView.Items.Clear();
			if (issues != null && issues.Count > 0) {
				jiraListView.Columns.Clear();
				LangKey[] columns = { LangKey.column_id, LangKey.column_created, LangKey.column_assignee, LangKey.column_reporter, LangKey.column_summary };
				foreach (LangKey column in columns) {
					jiraListView.Columns.Add(Language.GetString("jira", column));
				}
				foreach (Issue issue in issues) {
					ListViewItem item = new ListViewItem(issue.Key);
					item.Tag = issue;
					if (issue.Fields.Updated != null) {
						item.SubItems.Add(issue.Fields.Updated.ToLocalTime().ToString("d", DateTimeFormatInfo.InvariantInfo));
					} else {
						item.SubItems.Add("n.a.");
					}
					if (issue.Fields.Assignee != null) {
						item.SubItems.Add(issue.Fields.Assignee.DisplayName);
					} else {
						item.SubItems.Add("n.a.");
					}
					if (issue.Fields.Reporter != null) {
						item.SubItems.Add(issue.Fields.Reporter.DisplayName);
					} else {
						item.SubItems.Add("n.a.");
					}
					item.SubItems.Add(issue.Fields.Summary);
					jiraListView.Items.Add(item);
				}
				for (int i = 0; i < columns.Length; i++) {
					jiraListView.AutoResizeColumn(i, ColumnHeaderAutoResizeStyle.ColumnContent);
				}
			}
			jiraListView.EndUpdate();
			jiraListView.Refresh();
		}

		private void jiraListView_SelectedIndexChanged(object sender, EventArgs e) {
			if (jiraListView.SelectedItems != null && jiraListView.SelectedItems.Count > 0) {
				selectedIssue = (Issue)jiraListView.SelectedItems[0].Tag;
				jiraKey.Text = selectedIssue.Key;
				uploadButton.Enabled = true;
			} else {
				uploadButton.Enabled = false;
			}
		}

		private void uploadButton_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.OK;
		}

		private void cancelButton_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.Cancel;
		}

		private void jiraListView_ColumnClick(object sender, ColumnClickEventArgs e) {
			// Determine if clicked column is already the column that is being sorted.
			if (e.Column == columnSorter.SortColumn) {
				// Reverse the current sort direction for this column.
				if (columnSorter.Order == SortOrder.Ascending) {
					columnSorter.Order = SortOrder.Descending;
				} else {
					columnSorter.Order = SortOrder.Ascending;
				}
			} else {
				// Set the column number that is to be sorted; default to ascending.
				columnSorter.SortColumn = e.Column;
				columnSorter.Order = SortOrder.Ascending;
			}

			// Perform the sort with these new sort options.
			this.jiraListView.Sort();
		}

		void JiraKeyTextChanged(object sender, EventArgs e) {
			string jiranumber = jiraKey.Text;
			uploadButton.Enabled = false;
			int dashIndex = jiranumber.IndexOf('-');
			if (dashIndex > 0 && jiranumber.Length > dashIndex+1) {
				selectedIssue = jiraConnector.Details(jiraKey.Text).Result;
				if (selectedIssue != null) {
					uploadButton.Enabled = true;
				}
			}
		}
	}
}

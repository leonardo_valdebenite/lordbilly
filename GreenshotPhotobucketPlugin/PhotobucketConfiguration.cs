﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Windows;

using Greenshot.IniFile;
using GreenshotPlugin.Controls;
using GreenshotPlugin.Core;

namespace GreenshotPhotobucketPlugin {
	/// <summary>
	/// Description of PhotobucketConfiguration.
	/// </summary>
	[IniSection("Photobucket", Description="Greenshot Photobucket Plugin configuration")]
	public class PhotobucketConfiguration : ExportSettings {
		[IniProperty("UsePageLink", Description = "Use pagelink instead of direct link on the clipboard", DefaultValue = "False")]
		public bool UsePageLink {
			get;
			set;
		}

		[IniProperty("Token", Description = "The Photobucket token", Encrypted=true, ExcludeIfNull=true)]
		public string Token {
			get;
			set;
		}

		[IniProperty("TokenSecret", Description = "The Photobucket token secret", Encrypted=true, ExcludeIfNull=true)]
		public string TokenSecret {
			get;
			set;
		}

		[IniProperty("SubDomain", Description = "The Photobucket api subdomain", Encrypted = true, ExcludeIfNull = true)]
		public string SubDomain {
			get;
			set;
		}

		[IniProperty("Username", Description = "The Photobucket api username", ExcludeIfNull = true)]
		public string Username {
			get;
			set;
		}

		/// <summary>
		/// Property is stored in Greenshot constants
		/// </summary>
		[IniProperty("ConsumerKey", ReadOnly = true, Encrypted = true, Description = "Photobucket Consumer Key", ExcludeIfNull = true)]
		public string ConsumerKey {
			get;
			set;
		}

		/// <summary>
		/// Property is stored in Greenshot constants
		/// </summary>
		[IniProperty("ConsumerSecret", ReadOnly = true, Encrypted = true, Description = "Photobucket Consumer Secret", ExcludeIfNull = true)]
		public string ConsumerSecret {
			get;
			set;
		}

		public override void AfterLoad() {
			if (string.IsNullOrEmpty(ConsumerSecret) || string.IsNullOrEmpty(ConsumerKey)) {
				MessageBox.Show("Missing Photobucket credentials, upload to Photobuck won't work!", "Photobucket Plugin", MessageBoxButton.OK, MessageBoxImage.Information);
			}
		}
	}
}

Greenshot - a free screenshot tool optimized for productivity
=============================================================

Welcome to the source repository for Greenshot

What is Greenshot?
------------------

Greenshot is a light-weight screenshot software tool for Windows with the following key features:

* Quickly create screenshots of a selected region, window or fullscreen; you can even capture complete (scrolling) web pages from Internet Explorer.
* Easily annotate, highlight or obfuscate parts of the screenshot.
* Export the screenshot in various ways: save to file, send to printer, copy to clipboard, attach to e-mail, send Office programs or upload to photo sites like Flickr or Picasa, and others.
and a lot more options simplyfying creation of and work with screenshots every day.

Being easy to understand and configurable, Greenshot is an efficient tool for project managers, software developers, technical writers, testers and anyone else creating screenshots.



About this repository
---------------------
This repository has all the sources of Greenshot, but we have multiple branches of which two should be important to know about:

**1.1**
Greenshot 1.1.x can be found in the 1.1 branch, this branch can be considered stable.
But it mostly has a few more bug fixes and changes as are in our Greenshot release, as we collect multiple fixes before releasing.
1.1 is a "dying" branch, as we want to change a lot of the unterlying application for 2.0 and most of Greenshot will need to be rewitten.
This means pull request with fixes or **small** changes are very welcome!

**master**
The master branch has that which some day will be available in Greenshot 2.0.
As we are refactoring and changing a lot of code for 2.0, this branch should be considered highly unstable!
We accept pull-request for 2.0, but it might be better to wait with bigger changes until things settle down a bit.


Developing for Greenshot
------------------------
We develop Greenshot with Visual Studio Express 2013 and tested our solution on Visual Studio Professional 2012.
It should be possible to compile Greenshot directly after a checkout, eventuelly Visual Studio needs to upgrade the solution.
What doesn't work are the Plugins for cloud storage (like Box, Dropbox, Imgur, Picasa and Photobucket) as these need "API keys".
These keys are not in our Greenshot repository, if you want to develop on one of the plugins you will need to create you own keys by registering with theses services as a developer.
I will add a description here later to explain how include your API keys so you can develop.
